﻿namespace Application.UnitTests.Validation
{
    using System;
    using Application.Validation;
    using Common.Contracts;
    using Domain.Model;
    using FluentValidation.TestHelper;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class CardValidatorTests
    {
        [Test]
        public void Should_Have_Error_When_CVV_Is_Null()
        {
            var timeMock = new Mock<ITime>();
            timeMock.Setup(x => x.Today).Returns(DateTime.Today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card();
            var result = validator.TestValidate(card);
            result.ShouldHaveValidationErrorFor(x => x.CVV).WithErrorMessage("'CVV' must not be empty.");
        }

        [Test]
        public void Should_Have_Error_When_Number_Is_Null()
        {
            var timeMock = new Mock<ITime>();
            timeMock.Setup(x => x.Today).Returns(DateTime.Today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card();
            var result = validator.TestValidate(card);
            result.ShouldHaveValidationErrorFor(x => x.Number);
        }

        [Test]
        public void Should_Have_Error_When_ExpirationMonth_Is_LowerThan_1()
        {
            var timeMock = new Mock<ITime>();
            timeMock.Setup(x => x.Today).Returns(DateTime.Today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = 0
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldHaveValidationErrorFor(x => x.ExpirationMonth).WithErrorMessage("Invalid Expiration Date");
        }

        [Test]
        public void Should_Have_Error_When_ExpirationMonth_Is_GreaterThan_12()
        {
            var timeMock = new Mock<ITime>();
            timeMock.Setup(x => x.Today).Returns(DateTime.Today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = 13
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldHaveValidationErrorFor(x => x.ExpirationMonth).WithErrorMessage("Invalid Expiration Date");
        }

        [Test]
        public void Should_Have_Error_When_ExpirationMonth_Is_In_The_Past_Of_This_Year()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Parse("2020-04-12");
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month - 1,
                ExpirationYear = today.Year
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldHaveValidationErrorFor(x => x.ExpirationMonth).WithErrorMessage("Invalid Expiration Date");
        }

        [Test]
        public void Should_Not_Have_Error_When_ExpirationMonth_Is_Current_Month_AndExpirationYear_Is_CurrentYear()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Parse("2020-04-12");
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationMonth);
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationYear);
        }

        [Test]
        public void Should_Not_Have_Error_For_Valid_Card_This_Year()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Parse("2020-04-12");
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldNotHaveValidationErrorFor(x => x.Number);
            result.ShouldNotHaveValidationErrorFor(x => x.CVV);
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationMonth);
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationYear);
        }

        [Test]
        public void Should_Not_Have_Error_For_Valid_Card_With_Expiration_In_The_Future()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Parse("2020-04-12");
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new CardValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.AddYears(1).Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var result = validator.TestValidate(card, "*");
            result.ShouldNotHaveValidationErrorFor(x => x.Number);
            result.ShouldNotHaveValidationErrorFor(x => x.CVV);
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationMonth);
            result.ShouldNotHaveValidationErrorFor(x => x.ExpirationYear);
        }
    }
}
