﻿using System.Threading;
using Domain.Queries;
using MediatR;

namespace Application.UnitTests.Validation
{
    using System;
    using Application.Validation;
    using Common.Contracts;
    using Domain.Commands;
    using Domain.Model;
    using FluentValidation.TestHelper;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class IssuePaymentValidatorTests
    {
        [Test]
        public void Should_Have_Error_When_Payment_Is_Null()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var mediatorMock = new Mock<IMediator>();
            var validator = new IssuePaymentValidator(timeMock.Object, mediatorMock.Object);
            var command = new IssuePayment(null, Guid.NewGuid());
            var result = validator.TestValidate(command);
            result.ShouldHaveValidationErrorFor(x => x.Payment);
        }

        [Test]
        public void Should_Not_Have_Error_When_Command_Is_Valid()
        {
            var merchantId = Guid.NewGuid();
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var mediatorMock = new Mock<IMediator>();
            mediatorMock
                .Setup(x => x.Send(It.IsAny<GetMerchantById>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new Merchant()
                {
                    Id = merchantId,
                    Name = "Name"
                });

            var validator = new IssuePaymentValidator(timeMock.Object, mediatorMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment
            {
                Currency = "EUR",
                Card = card,
                Value = 10
            };

            var command = new IssuePayment(payment, merchantId);
            var result = validator.TestValidate(command);
            result.ShouldNotHaveValidationErrorFor(x => x.Payment);
        }
    }
}