﻿namespace Application.UnitTests.Validation
{
    using System;
    using Application.Validation;
    using Common.Contracts;
    using Domain.Model;
    using FluentValidation.TestHelper;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PaymentValidatorTests
    {
        [Test]
        public void Should_Have_Error_When_Value_Is_Zero()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new PaymentValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment
            {
                Card = card,
                Currency = "EUR"
            };
            var result = validator.TestValidate(payment);
            result.ShouldHaveValidationErrorFor(x => x.Value);
            result.ShouldNotHaveValidationErrorFor(x => x.Card);
            result.ShouldNotHaveValidationErrorFor(x => x.Currency);
        }

        [Test]
        public void Should_Have_Error_When_Currency_Is_Unknown()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new PaymentValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment
            {
                Value = 1,
                Card = card,
                Currency = "UNKNOWN"
            };
            var result = validator.TestValidate(payment);
            result.ShouldNotHaveValidationErrorFor(x => x.Value);
            result.ShouldNotHaveValidationErrorFor(x => x.Card);
            result.ShouldHaveValidationErrorFor(x => x.Currency);
        }

        [Test]
        public void Should_Have_Error_When_Currency_Is_Null()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new PaymentValidator(timeMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment
            {
                Value = 1,
                Card = card,
                Currency = null
            };
            var result = validator.TestValidate(payment);
            result.ShouldNotHaveValidationErrorFor(x => x.Value);
            result.ShouldNotHaveValidationErrorFor(x => x.Card);
            result.ShouldHaveValidationErrorFor(x => x.Currency);
        }

        [Test]
        public void Should_Have_Error_When_Card_Is_Null()
        {
            var timeMock = new Mock<ITime>();
            var today = DateTime.Today;
            timeMock.Setup(x => x.Today).Returns(today);
            var validator = new PaymentValidator(timeMock.Object);
            var payment = new Payment
            {
                Value = 1,
                Card = null,
                Currency = "EUR"
            };
            var result = validator.TestValidate(payment);
            result.ShouldHaveValidationErrorFor(x => x.Card);
        }
    }
}
