﻿using Microsoft.Extensions.Logging;

namespace Application.UnitTests.CommandHandlers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.CommandHandlers;
    using Banks;
    using Banks.Commands;
    using Common.Contracts;
    using Domain.Commands;
    using Domain.Events;
    using Domain.Model;
    using FluentAssertions;
    using MediatR;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class IssuePaymentHandlerTests
    {
        [Test]
        public async Task Handle_WhenPaymentIsAccepted_PaymentResultIsOk()
        {
            var confirmationId = Guid.NewGuid().ToString();
            var mediatorMock = new Mock<IMediator>();
            mediatorMock
                .Setup(x => x.Send(It.IsAny<AuthorizePayment>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new AuthorizationResult()
                {
                    Status = Status.Accepted,
                    ConfirmationId = confirmationId
                });


            var today = DateTime.Today;
            var timeMock = new Mock<ITime>();
            timeMock.Setup(x => x.UtcNow).Returns(DateTime.UtcNow);
            var loggerMock = new Mock<ILogger<IssuePaymentHandler>>();
            var handler = new IssuePaymentHandler(mediatorMock.Object, timeMock.Object, loggerMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.AddYears(1).Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment()
            {
                Card = card,
                Currency = "EUR",
                Value = (decimal)12.45
            };

            var request = new IssuePayment(payment, Guid.NewGuid());
            var expectedResult = new PaymentResult(request.Id, true, null);
            var result = await handler.Handle(request, CancellationToken.None);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Test]
        public async Task Handle_WhenPaymentIsAccepted_CorrectNotificationIsRaised()
        {
            var confirmationId = Guid.NewGuid().ToString();

            var mediatorMock = new Mock<IMediator>();
            mediatorMock
                .Setup(x => x.Send(It.IsAny<AuthorizePayment>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new AuthorizationResult()
                {
                    Status = Status.Accepted,
                    ConfirmationId = confirmationId
                });


            var today = DateTime.Today;
            var timeMock = new Mock<ITime>();
            var utcNow = DateTime.UtcNow;
            timeMock.Setup(x => x.UtcNow).Returns(utcNow); 
            var loggerMock = new Mock<ILogger<IssuePaymentHandler>>();
            var handler = new IssuePaymentHandler(mediatorMock.Object, timeMock.Object, loggerMock.Object);
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.AddYears(1).Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Payment()
            {
                Card = card,
                Currency = "EUR",
                Value = (decimal)12.45
            };

            var request = new IssuePayment(payment, Guid.NewGuid());
            await handler.Handle(request, CancellationToken.None);

            Func<PaymentAccepted, bool> assert = (notification) =>
            {
                notification.Payment.Should().Be(payment);
                notification.ConfirmationId.Should().Be(confirmationId);
                notification.Id.Should().Be(request.Id);
                return notification.TimestampUtc == utcNow;
            };

            mediatorMock.Verify(x => x.Publish(
                It.Is<INotification>(n => n is PaymentAccepted && assert((PaymentAccepted)n)),
                It.Is<CancellationToken>(c => c == CancellationToken.None)));
        }

    }
}
