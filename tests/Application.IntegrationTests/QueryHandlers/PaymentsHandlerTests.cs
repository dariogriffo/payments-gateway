﻿namespace Application.IntegrationTests.QueryHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.CommandHandlers;
    using Application.IntegrationTests.Helpers;
    using Banks;
    using Banks.Commands;
    using Common;
    using Domain.Commands;
    using Domain.Model;
    using Domain.Queries;
    using FluentAssertions;
    using Infrastructure;
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Mongo2Go;
    using Moq;
    using NUnit.Framework;
    using ServiceCollection.Extensions.Modules;

    [TestFixture]
    public class PaymentsHandlerTests
    {
        [Test]
        public async Task Handle_WhenPaymentExists_CardNumberIsMasked()
        {
            using var runner = MongoDbRunner.Start();
            var services = new Microsoft.Extensions.DependencyInjection.ServiceCollection();

            var keys = new Dictionary<string, string>
            {
                {"ConnectionStrings:PaymentsGateway", runner.ConnectionString},
                {"Encryption:Salt",Guid.NewGuid() + Guid.NewGuid().ToString() },
                {"Encryption:Password",Guid.NewGuid() + Guid.NewGuid().ToString() }
            };
            var configuration = new ConfigurationBuilder().AddInMemoryCollection(keys).Build();
            services.AddSingleton<IConfiguration>(configuration);
            var confirmationId = Guid.NewGuid().ToString();
            var authorizePaymentHandler = new AuthorizePaymentHandler(confirmationId);
            services.AddScoped<IRequestHandler<AuthorizePayment, AuthorizationResult>>((s) => authorizePaymentHandler);
            services.RegisterModule<CommonModule>();
            services.RegisterModule<InfrastructureModule>();
            services.RegisterModule<ApplicationModule>();
            services.AddMediatR(typeof(InfrastructureModule), typeof(ApplicationModule));
            var loggerMock = new Mock<ILogger<IssuePaymentHandler>>();
            services.AddSingleton(loggerMock.Object);
            await using var provider = services.BuildServiceProvider();
            using var scope = provider.CreateScope();
            var mediator = scope.ServiceProvider.GetService<IMediator>();

            var merchantId = await mediator.Send(new CreateMerchant("test"), CancellationToken.None);

            var today = DateTime.Today;
            var card = new Card
            {
                ExpirationMonth = today.Month,
                ExpirationYear = today.Year,
                Number = "4111111111111111",
                CVV = "023"
            };
            var payment = new Domain.Model.Payment
            {
                Card = card,
                Currency = "EUR",
                Value = 12
            };

            const string expectedCardNumber = "************1111";

            var result = await mediator.Send(new IssuePayment(payment, merchantId), CancellationToken.None);
            result.Ok.Should().Be(true);

            var storedPayment = await mediator.Send(new GetPaymentById(result.Id), CancellationToken.None);
            storedPayment.CardNumber.Should().Be(expectedCardNumber);
        }
    }
}
