﻿namespace Application.IntegrationTests.Helpers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Banks;
    using Banks.Commands;
    using MediatR;

    public class AuthorizePaymentHandler : IRequestHandler<AuthorizePayment, AuthorizationResult>
    {
        private readonly string _confirmationId;

        public AuthorizePaymentHandler(string confirmationId)
        {
            _confirmationId = confirmationId;
        }

        public Task<AuthorizationResult> Handle(AuthorizePayment request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new AuthorizationResult()
            {
                ConfirmationId = _confirmationId,
                Status = Status.Accepted,

            });
        }
    }
}