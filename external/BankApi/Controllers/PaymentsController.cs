﻿namespace BankApi.Controllers
{
    using System;
    using BankApi.Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using AuthorizationResult = BankApi.Models.AuthorizationResult;

    [ApiController]
    [Route("[controller]")]
    [Authorize(Policy = "BasicAuthentication")]
    public class PaymentsController : ControllerBase
    {
        private static readonly Random Random = new Random(Environment.TickCount);

        [HttpPost]
        public IActionResult Post([FromBody] Payment payment)
        {
            var next = Random.Next(1, 40);
            if (next < 2)
            {
                // Let's simulate some error
                var error = new AuthorizationResult()
                {
                    ConfirmationId = Guid.NewGuid().ToString(),
                    Status = Status.Error,
                    RejectedReason = $"Random Error {Guid.NewGuid().ToString()}",
                };

                return StatusCode(500, error);
            }

            if (next < 4)
            {
                var unauthorizedCard = new AuthorizationResult()
                {
                    ConfirmationId = Guid.NewGuid().ToString(),
                    Status = Status.Rejected,
                    RejectedReason = string.Empty,
                };

                return Unauthorized(unauthorizedCard);
            }

            var result = new AuthorizationResult()
            {
                ConfirmationId = Guid.NewGuid().ToString(),
                Status = Status.Accepted,
            };

            return Accepted(result);
        }
    }
}
