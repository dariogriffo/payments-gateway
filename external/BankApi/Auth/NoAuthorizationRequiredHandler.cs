namespace BankApi.Auth
{
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;

    public class NoAuthorizationRequiredHandler : AuthorizationHandler<NoAuthorizationRequired>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, NoAuthorizationRequired requirement)
        {
            if (context.User.Claims.Any() == false)
            {
                context.Fail();
            }
            else
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
