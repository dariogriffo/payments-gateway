namespace BankApi.Auth
{
    using Microsoft.AspNetCore.Authorization;

    public class NoAuthorizationRequired : IAuthorizationRequirement
    {
    }
}