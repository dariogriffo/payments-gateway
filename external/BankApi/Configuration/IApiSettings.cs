namespace BankApi.Configuration
{
    public interface IApiSettings
    {
        string User { get; }

        string Password { get; }
    }
}