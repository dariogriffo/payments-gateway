namespace BankApi.Configuration
{
    using System;
    using Microsoft.Extensions.Configuration;

    public class ApiSettings : IApiSettings
    {
        public ApiSettings(IConfiguration configuration)
        {
            var section = configuration.GetSection("Authentication") ?? throw new ArgumentException("AuthenticationSection");
            section.Bind(this);
        }

        public string User { get; set; }

        public string Password { get; set; }
    }
}
