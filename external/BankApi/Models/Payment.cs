﻿namespace BankApi.Models
{
    public class Payment
    {
        public string Number { get; set; }

        public int ExpirationMonth { get; set; }

        public int ExpirationYear { get; set; }

        public string CVV { get; set; }

        public decimal Value { get; set; }

        public string Currency { get; set; }
    }
}
