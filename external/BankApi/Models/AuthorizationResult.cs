﻿namespace BankApi.Models
{
    public enum Status
    {
        Accepted,
        Rejected,
        Error
    }

    public class AuthorizationResult
    {
        public Status Status { get; set; }

        public string ConfirmationId { get; set; }

        public string ErrorReason { get; set; }

        public string RejectedReason { get; set; }
    }
}