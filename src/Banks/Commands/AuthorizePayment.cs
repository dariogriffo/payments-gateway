﻿namespace Banks.Commands
{
    using Domain.Model;
    using MediatR;

    public class AuthorizePayment : IRequest<AuthorizationResult>
    {
        public AuthorizePayment(Payment payment)
        {
            Payment = payment;
        }

        public Payment Payment { get; }
    }
}
