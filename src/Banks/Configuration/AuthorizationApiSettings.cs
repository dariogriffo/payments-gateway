﻿namespace Banks.Configuration
{
    using Microsoft.Extensions.Configuration;

    public class AuthorizationApiSettings : IAuthorizationApiSettings
    {
        private const string SectionName = "AuthorizationApi";

        public AuthorizationApiSettings(IConfiguration configuration)
        {
            var section = configuration.GetSection(SectionName);
            section.Bind(this);
        }

        public int ExceptionsAllowedBeforeBreaking { get; set; } = 2;

        public int DurationOfBreakInSecs { get; set; } = 30;

        public int DurationBeforeRetryInSecs { get; set; } = 50;

        public int RetryCount { get; set; } = 3;

        public string Url { get; set; }

        public string User { get; set; }

        public string Password { get; set; }
    }
}