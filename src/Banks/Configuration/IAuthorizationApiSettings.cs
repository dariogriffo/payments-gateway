﻿namespace Banks.Configuration
{
    public interface IAuthorizationApiSettings
    {
        string Url { get; }

        int DurationBeforeRetryInSecs { get; }

        int DurationOfBreakInSecs { get; }

        int ExceptionsAllowedBeforeBreaking { get; }

        int RetryCount { get; }

        string User { get; }

        string Password { get; }
    }
}
