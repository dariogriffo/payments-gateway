﻿namespace Banks
{
    public enum Status
    {
        Accepted,
        Rejected,
        Error
    }
}
