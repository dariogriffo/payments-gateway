﻿namespace Banks.CommandHandlers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Banks.Commands;
    using Banks.Configuration;
    using Flurl.Http;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using Polly;
    using Polly.CircuitBreaker;
    using Polly.Wrap;

    public class AuthorizePaymentsHandler
        : IRequestHandler<AuthorizePayment, AuthorizationResult>
    {
        private readonly string _authorizationUrl;
        private readonly AsyncPolicyWrap _policy;
        private readonly IAuthorizationApiSettings _apiSettings;
        private readonly ILogger<AuthorizePaymentsHandler> _logger;
        private readonly IMapper _mapper;

        public AuthorizePaymentsHandler(IAuthorizationApiSettings apiSettings, ILogger<AuthorizePaymentsHandler> logger, IMapper mapper)
        {
            _apiSettings = apiSettings;
            _logger = logger;
            _mapper = mapper;
            _authorizationUrl = apiSettings.Url;

            var retryWait = TimeSpan.FromSeconds(apiSettings.DurationBeforeRetryInSecs);
            var breakWait = TimeSpan.FromSeconds(apiSettings.DurationOfBreakInSecs);

            var waitAndRetryPolicy =
                Policy
                    .Handle<Exception>(e => !(e is BrokenCircuitException))
                    .WaitAndRetryAsync(
                        apiSettings.RetryCount,
                        attempt => retryWait,
                        onRetryAsync: OnApiRequestError);

            var circuitBreakerPolicy =
                Policy
                    .Handle<Exception>()
                    .CircuitBreakerAsync(
                        apiSettings.ExceptionsAllowedBeforeBreaking,
                        breakWait,
                        onBreak: OnCircuitOpened,
                        onReset: OnCircuitClosed);

            _policy = retryWait > breakWait ?
                Policy.WrapAsync(waitAndRetryPolicy, circuitBreakerPolicy) :
                Policy.WrapAsync(circuitBreakerPolicy, waitAndRetryPolicy);
        }

        public async Task<AuthorizationResult> Handle(AuthorizePayment request, CancellationToken cancellationToken)
        {
            var model = _mapper.Map<Models.Payment>(request.Payment);
            Task<AuthorizationResult> Func()
            {
                return _authorizationUrl
                    .WithBasicAuth(_apiSettings.User, _apiSettings.Password)
                    .PostJsonAsync(model, cancellationToken)
                    .ReceiveJson<AuthorizationResult>();
            }

            return await _policy.ExecuteAsync(Func);
        }

        private Task OnApiRequestError(Exception exception, TimeSpan timeSpan)
        {
            _logger.LogWarning(exception, "Error calling Api. Waiting {timeSpan}", timeSpan);
            var ex = exception.InnerException;
            while (ex != null)
            {
                _logger.LogWarning(ex, "Inner Exception", timeSpan);
                ex = exception.InnerException;
            }

            return Task.CompletedTask;
        }

        private void OnCircuitOpened(Exception exception, TimeSpan timeSpan)
        {
            _logger.LogWarning(exception, "Error calling Api. Breaking connection");
        }

        private void OnCircuitClosed()
        {
            _logger.LogInformation("Api back online");
        }
    }
}
