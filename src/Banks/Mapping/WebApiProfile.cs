﻿namespace Banks.Mapping
{
    using AutoMapper;

    public class BanksProfile : Profile
    {
        public BanksProfile()
        {
            CreateMap<Domain.Model.Payment, Models.Payment>()
                .ForMember(x => x.Number, opt => opt.MapFrom(src => src.Card.Number))
                .ForMember(x => x.CVV, opt => opt.MapFrom(src => src.Card.CVV))
                .ForMember(x => x.ExpirationMonth, opt => opt.MapFrom(src => src.Card.ExpirationMonth))
                .ForMember(x => x.ExpirationYear, opt => opt.MapFrom(src => src.Card.ExpirationYear));
        }
    }
}
