﻿namespace Banks
{
    using Banks.CommandHandlers;
    using Banks.Commands;
    using Banks.Configuration;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;

    public class BanksModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddScoped<IRequestHandler<AuthorizePayment, AuthorizationResult>, AuthorizePaymentsHandler>();
            services.AddSingleton<IAuthorizationApiSettings, AuthorizationApiSettings>();
        }
    }
}
