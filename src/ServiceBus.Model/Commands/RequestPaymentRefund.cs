﻿namespace ServiceBus.Model.Commands
{
    using ProtoBuf;

    [ProtoContract]
    public class RequestPaymentRefund
    {
        [ProtoMember(1)]
        public string ConfirmationId { get; set; }
    }
}
