﻿namespace Domain.Queries
{
    using System;
    using Domain.Model;
    using MediatR;

    public class GetMerchantById : IRequest<Merchant>
    {
        public GetMerchantById(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}