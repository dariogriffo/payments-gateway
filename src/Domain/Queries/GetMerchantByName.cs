﻿using Domain.Model;
using MediatR;

namespace Domain.Queries
{
    public class GetMerchantByName : IRequest<Merchant>
    {
        public GetMerchantByName(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}