﻿namespace Domain.Queries
{
    using System;
    using System.Collections.Generic;
    using MediatR;

    public class GetMerchantPayments : IRequest<IEnumerable<Domain.Queries.Models.Payment>>
    {
        public GetMerchantPayments(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
