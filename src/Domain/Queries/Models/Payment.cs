﻿using System;

namespace Domain.Queries.Models
{
    public class Payment
    {
        public Payment(string cardNumber, int expirationMonth, int expirationYear, decimal value, string confirmationId, string error)
        {
            CardNumber = cardNumber;
            ExpirationMonth = expirationMonth;
            ExpirationYear = expirationYear;
            Error = error;
            Success = String.IsNullOrEmpty(error);
            ConfirmationId = confirmationId;
            Value = value;
        }

        public string CardNumber { get; }

        public int ExpirationMonth { get; }

        public int ExpirationYear { get; }

        public bool Success { get; }

        public string ConfirmationId { get; }

        public string Error { get; }

        public decimal Value { get; }
    }
}