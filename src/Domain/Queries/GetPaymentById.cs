﻿namespace Domain.Queries
{
    using System;
    using Domain.Queries.Models;
    using MediatR;
    
    public class GetPaymentById : IRequest<Payment>
    {
        public GetPaymentById(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
