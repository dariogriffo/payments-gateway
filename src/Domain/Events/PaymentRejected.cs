﻿namespace Domain.Events
{
    using System;
    using Domain.Model;

    public class PaymentRejected : IEvent
    {
        public PaymentRejected(Guid id, Payment payment, Guid merchantId, string reason, DateTime timestampUtc)
        {
            Id = id;
            Payment = payment;
            MerchantId = merchantId;
            Reason = reason;
            TimestampUtc = timestampUtc;
        }

        public Guid Id { get; }

        public Payment Payment { get; }

        public Guid MerchantId { get; }

        public string Reason { get; }

        public DateTime TimestampUtc { get; }
    }
}
