﻿namespace Domain.Events
{
    using System;
    using MediatR;

    public interface IEvent : INotification
    {
        DateTime TimestampUtc { get; }
    }
}