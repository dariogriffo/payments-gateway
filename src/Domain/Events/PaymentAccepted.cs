﻿namespace Domain.Events
{
    using System;
    using Domain.Model;

    public class PaymentAccepted : IEvent
    {
        public PaymentAccepted(Guid id, Payment payment, Guid merchantId, string confirmationId, DateTime timestampUtc)
        {
            Id = id;
            Payment = payment;
            MerchantId = merchantId;
            ConfirmationId = confirmationId;
            TimestampUtc = timestampUtc;
        }

        public Guid Id { get; }
        public Payment Payment { get; }

        public Guid MerchantId { get; }

        public string ConfirmationId { get; }

        public DateTime TimestampUtc { get; }
    }
}