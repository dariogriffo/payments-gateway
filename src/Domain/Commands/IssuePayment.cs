﻿namespace Domain.Commands
{
    using System;
    using Domain.Model;
    using MediatR;

    public class IssuePayment : IRequest<PaymentResult>
    {
        public IssuePayment(Payment payment, Guid merchantId)
        {
            Payment = payment;
            MerchantId = merchantId;
            Id = Guid.NewGuid();
        }

        public Payment Payment { get; }

        public Guid MerchantId { get; }

        public Guid Id { get; }
    }
}
