﻿namespace Domain.Commands
{
    using System;
    using MediatR;

    public class CreateMerchant : IRequest<Guid>
    {
        public CreateMerchant(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}