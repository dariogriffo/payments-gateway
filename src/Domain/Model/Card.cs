﻿namespace Domain.Model
{
    public class Card
    {
        public string Number { get; set; }

        public int ExpirationMonth { get; set; }

        public int ExpirationYear { get; set; }

        public string CVV { get; set; }
    }
}
