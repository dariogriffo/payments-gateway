﻿namespace Domain.Model
{
    using System;

    public class Merchant
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}