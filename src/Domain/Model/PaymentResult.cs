﻿namespace Domain.Model
{
    using System;

    public class PaymentResult
    {
        public PaymentResult(Guid id, bool ok, string failedReason)
        {
            Id = id;
            Ok = ok;
            FailedReason = failedReason;
        }

        public Guid Id { get; }

        public bool Ok { get; }

        public string FailedReason { get; }
    }
}