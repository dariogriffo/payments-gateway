﻿namespace Domain.Model
{
    public class Payment
    {
        public Card Card { get; set; }

        public decimal Value { get; set; }

        public string Currency { get; set; }
    }
}
