namespace WebApi
{
    using Application;
    using AutoMapper;
    using Banks;
    using Banks.Mapping;
    using Common;
    using FluentValidation.AspNetCore;
    using Infrastructure;
    using MediatR;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using ServiceCollection.Extensions.Modules;
    using WebApi.Mapping;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMvc().AddFluentValidation();

            services.RegisterModule<CommonModule>();
            services.RegisterModule<BanksModule>();
            services.RegisterModule<InfrastructureModule>();
            services.RegisterModule<ApplicationModule>();

            services.AddAutoMapper(typeof(WebApiProfile), typeof(BanksProfile));
            services.AddMediatR(typeof(BanksModule), typeof(InfrastructureModule), typeof(ApplicationModule));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
