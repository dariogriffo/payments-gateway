﻿using System;

namespace WebApi.Models
{
    public class Payment
    {
        public decimal Value { get; set; }

        public string Currency { get; set; }

        public string CardNumber { get; set; }

        public int CardExpirationMonth { get; set; }

        public int CardExpirationYear { get; set; }

        public string CardCVV { get; set; }
        
        public Guid MerchantId { get; set; }
    }
}
