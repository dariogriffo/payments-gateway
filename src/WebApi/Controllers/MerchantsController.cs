﻿using System;
using Domain.Queries;
using FluentValidation;

namespace WebApi.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Commands;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    [Route("[controller]")]
    public class MerchantsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MerchantsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("{name}")]
        public async Task<IActionResult> Post(string name, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(new CreateMerchant(name), cancellationToken);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("{id}/payments")]
        public async Task<IActionResult> Post(Guid id, CancellationToken cancellationToken)
        {
            var payments = await _mediator.Send(new GetMerchantPayments(id), cancellationToken);
            return Ok(payments);
        }
    }
}
