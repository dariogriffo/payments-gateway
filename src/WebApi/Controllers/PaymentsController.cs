﻿namespace WebApi.Controllers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Domain.Commands;
    using Domain.Queries;
    using FluentValidation;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [ApiController]
    [Route("[controller]")]
    public class PaymentsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly ILogger<PaymentsController> _logger;

        public PaymentsController(IMediator mediator, IMapper mapper, ILogger<PaymentsController> logger)
        {
            _mediator = mediator;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(Guid id, CancellationToken cancellationToken)
        {
            var payment = await _mediator.Send(new GetPaymentById(id), cancellationToken);
            if (payment == null)
            {
                return NotFound(id);
            }

            return Ok(payment);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Models.Payment model, CancellationToken cancellationToken)
        {
            var payment = _mapper.Map<Domain.Model.Payment>(model);
            var command = new IssuePayment(payment, model.MerchantId);
            _logger.LogDebug("Issuing a payment for merchant {0}", command.MerchantId);
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                if (result.Ok)
                {
                    _logger.LogDebug("Payment for merchant {0} created with id", command.MerchantId, result.Id);
                }
                else
                {
                    _logger.LogWarning("Error while trying to issue a payment {0}", result.FailedReason);
                }
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                _logger.LogWarning(ex, "Validation error while trying to issue a payment");
                return BadRequest(ex.Message);
            }
        }
    }
}
