﻿namespace WebApi.Mapping
{
    using AutoMapper;
    using Domain.Model;

    public class WebApiProfile : Profile
    {
        public WebApiProfile()
        {
            CreateMap<Models.Payment, Domain.Model.Payment>()
                .ForMember(x => x.Card, opt => opt.MapFrom(src => new Card()
                {
                    Number = src.CardNumber,
                    CVV = src.CardCVV,
                    ExpirationMonth = src.CardExpirationMonth,
                    ExpirationYear = src.CardExpirationYear,
                }));
        }
    }
}
