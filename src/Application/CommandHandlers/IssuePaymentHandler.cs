﻿namespace Application.CommandHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Banks;
    using Banks.Commands;
    using Common.Contracts;
    using Domain.Commands;
    using Domain.Events;
    using Domain.Model;
    using MediatR;
    using Microsoft.Extensions.Logging;

    public class IssuePaymentHandler
        : IRequestHandler<IssuePayment, PaymentResult>
    {
        private readonly IMediator _mediator;
        private readonly ITime _time;
        private readonly ILogger<IssuePaymentHandler> _logger;

        public IssuePaymentHandler(IMediator mediator, ITime time, ILogger<IssuePaymentHandler> logger)
        {
            _mediator = mediator;
            _time = time;
            _logger = logger;
        }

        public async Task<PaymentResult> Handle(IssuePayment request, CancellationToken cancellationToken)
        {
            var authorization = await _mediator.Send(new AuthorizePayment(request.Payment), cancellationToken);

            var logMessage = authorization.Status switch
            {
                Status.Rejected => $"/{authorization.RejectedReason}",
                Status.Error => $"/{authorization.ErrorReason}",
                Status.Accepted => string.Empty,
                _ => string.Empty,
            };

            _logger.LogInformation("Authorization for payment with id {0} was {1}{2}", request.Id, authorization.Status.ToString(), logMessage);

            var notification = authorization.Status switch
            {
                Status.Rejected => (INotification)new PaymentRejected(request.Id, request.Payment, request.MerchantId, authorization.RejectedReason, _time.UtcNow),
                Status.Accepted => new PaymentAccepted(request.Id, request.Payment, request.MerchantId, authorization.ConfirmationId, _time.UtcNow),
                Status.Error => new PaymentFailed(request.Id, request.Payment, request.MerchantId, authorization.ErrorReason, _time.UtcNow),
                _ => new PaymentFailed(request.Id, request.Payment, request.MerchantId, authorization.ErrorReason, _time.UtcNow),
            };

            var result = authorization.Status switch
            {
                Status.Rejected => new PaymentResult(request.Id, false, authorization.RejectedReason),
                Status.Accepted => new PaymentResult(request.Id, true, null),
                _ => new PaymentResult(request.Id, false, authorization.ErrorReason),
            };

            await _mediator.Publish(notification, cancellationToken);

            return result;
        }
    }
}
