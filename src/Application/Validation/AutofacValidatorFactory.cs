namespace Application.Validation
{
    using System;
    using FluentValidation;

    public class AutofacValidatorFactory : ValidatorFactoryBase
    {
        private readonly IServiceProvider _context;

        public AutofacValidatorFactory(IServiceProvider context)
        {
            _context = context;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return _context.GetService(validatorType) as IValidator;
        }
    }
}
