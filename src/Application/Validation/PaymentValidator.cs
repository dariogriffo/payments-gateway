﻿namespace Application.Validation
{
    using System.Linq;
    using Common.Contracts;
    using Domain.Model;
    using FluentValidation;

    public class PaymentValidator : AbstractValidator<Payment>
    {
        private static readonly string[] ValidCurrencies = { "EUR", "USD" };

        public PaymentValidator(ITime time)
        {
            RuleFor(x => x.Card).NotNull();
            RuleFor(x => x.Card).SetValidator(new CardValidator(time)).When(x => x.Card != null);
            RuleFor(x => x.Currency).NotNull().NotEmpty().Must(BeKnownCurrency);
            RuleFor(x => x.Value).GreaterThan(0);
        }

        private bool BeKnownCurrency(string arg)
        {
            return ValidCurrencies.Any(x => x == arg);
        }
    }
}
