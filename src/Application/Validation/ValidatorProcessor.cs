using System.Threading;
using System.Threading.Tasks;
using Domain.Commands;
using FluentValidation;
using MediatR.Pipeline;

namespace Application.Validation
{
    public class ValidatorProcessor
        : IRequestPreProcessor<IssuePayment>,
        IRequestPreProcessor<CreateMerchant>
    {
        private readonly IValidatorFactory _factory;

        public ValidatorProcessor(IValidatorFactory factory)
        {
            _factory = factory;
        }

        public Task Process(IssuePayment request, CancellationToken cancellationToken)
        {
            return InternalProcess(request, cancellationToken);
        }

        public Task Process(CreateMerchant request, CancellationToken cancellationToken)
        {
            return InternalProcess(request, cancellationToken);
        }

        private Task InternalProcess<T>(T request, CancellationToken cancellationToken)
        {
            var validator = _factory.GetValidator(typeof(T));
            if (validator == null)
            {
                return Task.CompletedTask;
            }

            var result = validator.Validate(request);
            if (result.IsValid)
            {
                return Task.CompletedTask;
            }

            throw new ValidationException(result.Errors);
        }
    }
}
