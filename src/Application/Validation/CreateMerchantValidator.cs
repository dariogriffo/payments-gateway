﻿namespace Application.Validation
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Commands;
    using Domain.Queries;
    using FluentValidation;
    using MediatR;


    public class CreateMerchantValidator : AbstractValidator<CreateMerchant>
    {
        private readonly IMediator _mediator;

        public CreateMerchantValidator(IMediator mediator)
        {
            _mediator = mediator;
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Name).MustAsync(BeUnique).WithMessage("Merchant already exists");
        }

        private async Task<bool> BeUnique(string name, CancellationToken cancellationToken)
        {
            var merchant = await _mediator.Send(new GetMerchantByName(name), cancellationToken);
            return merchant == null;
        }
    }
}
