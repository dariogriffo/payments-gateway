﻿namespace Application.Validation
{
    using System;
    using Common.Contracts;
    using Domain.Model;
    using FluentValidation;
    using FluentValidation.Validators;

    public class CardValidator : AbstractValidator<Card>
    {
        public CardValidator(ITime time)
        {
            RuleFor(x => x.CVV).NotNull().NotEmpty();
            RuleFor(x => x.Number).NotNull().SetValidator(new CreditCardValidator());
            RuleSet("ExpirationDate", () =>
            {
                RuleFor(x => x.ExpirationMonth).Must((card, month, context) =>
                {
                    if (month > 12)
                    {
                        return false;
                    }

                    if (month < 1)
                    {
                        return false;
                    }

                    if (card.ExpirationYear == DateTime.Today.Year)
                    {
                        return month >= time.Today.Month;
                    }

                    return true;
                }).WithMessage("Invalid Expiration Date");
                RuleFor(x => x.ExpirationYear).GreaterThanOrEqualTo(DateTime.Today.Year).WithMessage("Invalid Expiration Date");
            });
        }
    }
}
