﻿namespace Application.Validation
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Contracts;
    using Domain.Commands;
    using Domain.Queries;
    using FluentValidation;
    using MediatR;

    public class IssuePaymentValidator : AbstractValidator<IssuePayment>
    {
        private readonly IMediator _mediator;

        public IssuePaymentValidator(ITime time, IMediator mediator)
        {
            _mediator = mediator;
            RuleFor(x => x.MerchantId).MustAsync(Exists).WithMessage("Unknown merchant");;
            RuleFor(x => x.Payment).NotNull().SetValidator(new PaymentValidator(time));
        }

        private async Task<bool> Exists(Guid merchantId, CancellationToken cancellationToken)
        {
            var merchant = await _mediator.Send(new GetMerchantById(merchantId), cancellationToken);
            return merchant != null;
        }
    }
}
