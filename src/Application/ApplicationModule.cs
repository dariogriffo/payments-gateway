﻿namespace Application
{
    using Application.Validation;
    using Domain.Commands;
    using Domain.Model;
    using FluentValidation;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;

    public class ApplicationModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddScoped<IValidator<CreateMerchant>, CreateMerchantValidator>();
            services.AddScoped<IValidator<Card>, CardValidator>();
            services.AddScoped<IValidator<Payment>, PaymentValidator>();
            services.AddScoped<IValidator<IssuePayment>, IssuePaymentValidator>();
            services.AddScoped<IValidatorFactory, AutofacValidatorFactory>(); 
        }
    }
}
