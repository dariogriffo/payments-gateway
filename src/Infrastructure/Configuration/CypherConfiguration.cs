namespace Infrastructure.Configuration
{
    using System;
    using Microsoft.Extensions.Configuration;

    public class CypherConfiguration : ICypherConfiguration
    {
        private static readonly string SectionName = "Encryption";

        public CypherConfiguration(IConfiguration configuration)
        {
            var section = configuration.GetSection(SectionName);
            section.Bind(this);
            if (string.IsNullOrEmpty(Salt))
            {
                throw new ArgumentNullException(nameof(Salt));
            }

            if (string.IsNullOrEmpty(Password))
            {
                throw new ArgumentNullException(nameof(Password));
            }
        }

        public string Salt { get; set; }

        public string Password { get; set; }
    }
}
