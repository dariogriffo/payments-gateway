﻿namespace Infrastructure.Configuration
{
    public interface ICypherConfiguration
    {
        string Password { get; }

        string Salt { get; }
    }
}
