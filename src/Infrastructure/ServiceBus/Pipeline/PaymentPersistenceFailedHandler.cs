﻿using Microsoft.Extensions.Logging;
using ServiceBus.Model.Commands;

namespace Infrastructure.ServiceBus.Pipeline
{
    using System.Threading;
    using System.Threading.Tasks;
    using Infrastructure.Persistence.Events;
    using MediatR;
    using Rebus.Bus;
    
    public class PaymentPersistenceFailedHandler
        : INotificationHandler<PaymentPersistenceFailed>
    {
        private readonly IBus _bus;
        private readonly ILogger _logger;

        public PaymentPersistenceFailedHandler(IBus bus, ILogger<PaymentPersistenceFailedHandler> logger)
        {
            _bus = bus;
            _logger = logger;
        }

        public Task Handle(PaymentPersistenceFailed notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting refund request with confirmation id {0} for payment with id {1}", notification.ConfirmationId, notification.PaymentId);
            var command = new RequestPaymentRefund()
            {
                ConfirmationId = notification.ConfirmationId
            };
            return _bus.Send(command);
        }
    }
}
