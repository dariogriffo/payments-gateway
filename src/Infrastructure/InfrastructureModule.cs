﻿using ServiceBus.Model.Commands;

namespace Infrastructure
{
    using Infrastructure.Concrete;
    using Infrastructure.Configuration;
    using Infrastructure.Contracts;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using MongoDB.Driver;
    using Rebus.Routing.TypeBased;
    using Rebus.ServiceProvider;
    using Rebus.Transport.InMem;
    using ServiceCollection.Extensions.Modules;

    public class InfrastructureModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton(typeof(IMongoClient), f => new MongoClient(f.GetService<IConfiguration>().GetConnectionString("PaymentsGateway")));
            services.AddSingleton<ICypher, Cyhper>();
            services.AddSingleton<ICypherConfiguration, CypherConfiguration>();

            services.AddRebus(configure => configure
                .Transport(t => t.UseInMemoryTransport(new InMemNetwork(), "Messages"))
                .Routing(r => r.TypeBased().MapAssemblyOf<RequestPaymentRefund>("Messages")));
        }
    }
}
