namespace Infrastructure.Concrete
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using Infrastructure.Configuration;
    using Infrastructure.Contracts;

    public class Cyhper : ICypher
    {
        private readonly ICypherConfiguration _configuration;

        public Cyhper(ICypherConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Encrypt(string value)
        {
            var algorithm = new RijndaelManaged();
            using (var passwordKey = new Rfc2898DeriveBytes(_configuration.Password, Encoding.UTF8.GetBytes(_configuration.Salt)))
            {
                algorithm.Key = passwordKey.GetBytes(algorithm.KeySize / 8);
                algorithm.IV = passwordKey.GetBytes(algorithm.BlockSize / 8);
            }

            string result;
            using (var encryptor = algorithm.CreateEncryptor())
            {
                using var ms = new MemoryStream();
                using var crStream = new CryptoStream(ms, encryptor, CryptoStreamMode.Write);
                var data = Encoding.ASCII.GetBytes(value);
                crStream.Write(data, 0, data.Length);
                crStream.Flush();
                crStream.FlushFinalBlock();
                result = Convert.ToBase64String(ms.ToArray());
            }

            return result;
        }

        public string Decrypt(string value)
        {
            var algorithm = new RijndaelManaged();
            using (var passwordKey = new Rfc2898DeriveBytes(_configuration.Password, Encoding.UTF8.GetBytes(_configuration.Salt)))
            {
                algorithm.Key = passwordKey.GetBytes(algorithm.KeySize / 8);
                algorithm.IV = passwordKey.GetBytes(algorithm.BlockSize / 8);
            }

            using var decryptor = algorithm.CreateDecryptor();
            using var ms = new MemoryStream(Convert.FromBase64String(value));
            using var crStream = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);
            string result;
            using (var sr = new StreamReader(crStream))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }
    }
}
