﻿namespace Infrastructure.Contracts
{
    public interface ICypher
    {
        string Encrypt(string value);

        string Decrypt(string value);
    }
}