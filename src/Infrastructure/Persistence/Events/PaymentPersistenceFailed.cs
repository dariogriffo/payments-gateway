﻿namespace Infrastructure.Persistence.Events
{
    using System;
    using Domain.Model;
    using MediatR;

    public class PaymentPersistenceFailed : INotification
    {
        public PaymentPersistenceFailed(Payment payment, Guid paymentId, string confirmationId, Exception exception)
        {
            Payment = payment;
            PaymentId = paymentId;
            ConfirmationId = confirmationId;
            Exception = exception;
        }

        public Payment Payment { get; }

        public Guid PaymentId { get; }

        public string ConfirmationId { get; }

        public Exception Exception { get; }
    }
}
