﻿namespace Infrastructure.Persistence.NotificationsHandlers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Commands;
    using Domain.Model;
    using MediatR;
    using MongoDB.Driver;

    public class MerchantsHandler
        : IRequestHandler<CreateMerchant, Guid>
    {
        private readonly IMongoClient _client;

        public MerchantsHandler(IMongoClient client)
        {
            _client = client;
        }

        public async Task<Guid> Handle(CreateMerchant request, CancellationToken cancellationToken)
        {
            var merchant = new Merchant()
            {
                Name = request.Name,
                Id = Guid.NewGuid()
            };
            await _client
                .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                .GetCollection<Merchant>(DatabaseConstants.MerchantsCollection)
                .InsertOneAsync(merchant, new InsertOneOptions() { BypassDocumentValidation = true }, cancellationToken);
            return merchant.Id;
        }
    }
}