﻿using System;

namespace Infrastructure.Persistence.NotificationsHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Events;
    using Domain.Model;
    using Infrastructure.Contracts;
    using Infrastructure.Persistence.Events;
    using Infrastructure.Persistence.Model;
    using MediatR;
    using MongoDB.Driver;
    using Payment = Infrastructure.Persistence.Model.Payment;

    public class PaymentsHandler
        : INotificationHandler<PaymentAccepted>,
        INotificationHandler<PaymentFailed>,
        INotificationHandler<PaymentRejected>
    {
        private readonly ICypher _cypher;
        private readonly IMongoClient _client;
        private readonly IMediator _mediator;

        public PaymentsHandler(ICypher cypher, IMongoClient client, IMediator mediator)
        {
            _cypher = cypher;
            _client = client;
            _mediator = mediator;
        }

        public Task Handle(PaymentAccepted notification, CancellationToken cancellationToken)
        {
            var payment = new Model.Payment
            {
                MerchantId = notification.MerchantId,
                Model = AdaptModel(notification.Payment),
                Id = notification.Id,
                ConfirmationId = notification.ConfirmationId,
                TimestampUtc = notification.TimestampUtc
            };
            return SavePayment(payment, notification.Payment, notification.Id, cancellationToken);
        }

        public Task Handle(PaymentFailed notification, CancellationToken cancellationToken)
        {
            var payment = new Model.Payment
            {
                MerchantId = notification.MerchantId,
                Model = AdaptModel(notification.Payment),
                Id = notification.Id,
                Error = new Error
                {
                    Status = "Failed",
                    Message = notification.Reason
                },
                TimestampUtc = notification.TimestampUtc
            };
            return SavePayment(payment, notification.Payment, notification.Id, cancellationToken);
        }

        public Task Handle(PaymentRejected notification, CancellationToken cancellationToken)
        {
            var payment = new Model.Payment
            {
                MerchantId = notification.MerchantId,
                Model = AdaptModel(notification.Payment),
                Id = notification.Id,
                Error = new Error
                {
                    Status = "Rejected",
                    Message = notification.Reason
                },
                TimestampUtc = notification.TimestampUtc
            };
            return SavePayment(payment, notification.Payment, notification.Id, cancellationToken);
        }

        private Domain.Model.Payment AdaptModel(Domain.Model.Payment payment)
        {
            return new Domain.Model.Payment
            {
                Currency = payment.Currency,
                Card = new Card
                {
                    Number = _cypher.Encrypt(payment.Card.Number),
                    CVV = payment.Card.CVV,
                    ExpirationMonth = payment.Card.ExpirationMonth,
                    ExpirationYear = payment.Card.ExpirationYear
                },
                Value = payment.Value
            };
        }

        private async Task SavePayment(Payment payment, Domain.Model.Payment originalPayment, Guid paymentId, CancellationToken cancellationToken)
        {
            try
            {
                await _client
                    .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                    .GetCollection<Model.Payment>(DatabaseConstants.PaymentsCollection)
                    .InsertOneAsync(payment, new InsertOneOptions() { BypassDocumentValidation = true }, cancellationToken);
            }
            catch (MongoException ex)
            {
                var paymentPersistenceFailed = new PaymentPersistenceFailed(originalPayment, paymentId, payment.ConfirmationId, ex);
                await _mediator.Publish(paymentPersistenceFailed, cancellationToken);
            }
        }
    }
}
