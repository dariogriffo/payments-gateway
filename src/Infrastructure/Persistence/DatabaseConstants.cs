﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Application.IntegrationTests")]

namespace Infrastructure.Persistence
{
    public class DatabaseConstants
    {
        internal const string PaymentsGatewayDatabase = "paymentsgateway";
        internal const string PaymentsCollection = "payments";
        internal const string MerchantsCollection = "merchants";
    }
}