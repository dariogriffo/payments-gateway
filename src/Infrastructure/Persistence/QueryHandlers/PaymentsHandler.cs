﻿using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Persistence.QueryHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Contracts;
    using Domain.Queries;
    using Infrastructure.Contracts;
    using MediatR;
    using MongoDB.Driver;
    using MongoDB.Driver.Linq;
    using Payment = Infrastructure.Persistence.Model.Payment;

    public class PaymentsHandler
        : IRequestHandler<GetPaymentById, Domain.Queries.Models.Payment>,
        IRequestHandler<GetMerchantPayments, IEnumerable<Domain.Queries.Models.Payment>>
    {
        private readonly ICardNumberMasker _masker;
        private readonly ICypher _cypher;
        private readonly IMongoClient _client;

        public PaymentsHandler(ICardNumberMasker masker, IMongoClient client, ICypher cypher)
        {
            _masker = masker;
            _client = client;
            _cypher = cypher;
        }

        public async Task<Domain.Queries.Models.Payment> Handle(GetPaymentById request, CancellationToken cancellationToken)
        {
            var payment =
                await _client
                        .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                        .GetCollection<Payment>(DatabaseConstants.PaymentsCollection)
                        .AsQueryable()
                        .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);

            if (payment == null)
            {
                return null;
            }

            var card = payment.Model.Card;
            var maskedCard = _masker.Mask(_cypher.Decrypt(card.Number));
            return new Domain.Queries.Models.Payment(
                maskedCard,
                card.ExpirationMonth,
                card.ExpirationYear,
                payment.Model.Value,
                payment.ConfirmationId,
                payment.Error?.Message);
        }

        public async Task<IEnumerable<Domain.Queries.Models.Payment>> Handle(GetMerchantPayments request, CancellationToken cancellationToken)
        {
            var payments =
                await _client
                    .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                    .GetCollection<Payment>(DatabaseConstants.PaymentsCollection)
                    .AsQueryable()
                    .Where(x => x.MerchantId == request.Id)
                    .ToListAsync(cancellationToken);

            return from payment in payments
                let card = payment.Model.Card
                select new Domain.Queries.Models.Payment(
                    _masker.Mask(_cypher.Decrypt(card.Number)),
                    card.ExpirationMonth,
                    card.ExpirationYear,
                    payment.Model.Value,
                    payment.ConfirmationId,
                    payment.Error?.Message);
        }
    }
}
