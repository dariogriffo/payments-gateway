﻿namespace Infrastructure.Persistence.QueryHandlers
{
    using System.Threading;
    using System.Threading.Tasks;
    using Domain.Model;
    using Domain.Queries;
    using MediatR;
    using MongoDB.Driver;
    using MongoDB.Driver.Linq;

    public class MerchantsHandler
        : IRequestHandler<GetMerchantById, Merchant>,
        IRequestHandler<GetMerchantByName, Merchant>
    {
        private readonly IMongoClient _client;

        public MerchantsHandler(IMongoClient client)
        {
            _client = client;
        }

        public Task<Merchant> Handle(GetMerchantById request, CancellationToken cancellationToken)
        {
            return  _client
                .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                .GetCollection<Merchant>(DatabaseConstants.MerchantsCollection)
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        }

        public Task<Merchant> Handle(GetMerchantByName request, CancellationToken cancellationToken)
        {
            return _client
                .GetDatabase(DatabaseConstants.PaymentsGatewayDatabase)
                .GetCollection<Merchant>(DatabaseConstants.MerchantsCollection)
                .AsQueryable()
                .FirstOrDefaultAsync(x => x.Name == request.Name, cancellationToken);
        }
    }
}