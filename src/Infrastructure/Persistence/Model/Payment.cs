﻿namespace Infrastructure.Persistence.Model
{
    using System;

    public class Payment
    {
        public Guid Id { get; set; }

        public Guid MerchantId { get; set; }

        public Domain.Model.Payment Model { get; set; }

        public string ConfirmationId { get; set; }

        public Error Error { get; set; }

        public DateTime TimestampUtc { get; set; }
    }
}
