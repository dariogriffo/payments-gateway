﻿namespace Infrastructure.Persistence.Model
{
    public class Error
    {
        public string Status { get; set; }

        public string Message { get; set; }
    }
}