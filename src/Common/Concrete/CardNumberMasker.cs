﻿namespace Common.Concrete
{
    using Common.Contracts;

    public class CardNumberMasker : ICardNumberMasker
    {
        public string Mask(string cardNumber)
        {
            return string.Concat("".PadLeft(12, '*'), cardNumber.Substring(cardNumber.Length - 4));
        }
    }
}
