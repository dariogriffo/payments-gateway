namespace Common.Concrete
{
    using System;
    using Common.Contracts;

    public class Time : ITime
    {
        public DateTime Now => DateTime.Now;
        public DateTime UtcNow => DateTime.UtcNow;
        public DateTime Today => DateTime.Today;
    }
}
