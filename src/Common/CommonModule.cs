﻿namespace Common
{
    using Common.Concrete;
    using Common.Contracts;
    using Microsoft.Extensions.DependencyInjection;
    using ServiceCollection.Extensions.Modules;

    public class CommonModule : Module
    {
        protected override void Load(IServiceCollection services)
        {
            base.Load(services);
            services.AddSingleton<ITime, Time>();
            services.AddSingleton<ICardNumberMasker, CardNumberMasker>();
        }
    }
}
