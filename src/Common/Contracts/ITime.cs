namespace Common.Contracts
{
    using System;

    public interface ITime
    {
        DateTime Now { get; }

        DateTime UtcNow { get; }

        DateTime Today { get; }
    }
}
