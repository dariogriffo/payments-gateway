﻿namespace Common.Contracts
{
    public interface ICardNumberMasker
    {
        string Mask(string cardNumber);
    }
}